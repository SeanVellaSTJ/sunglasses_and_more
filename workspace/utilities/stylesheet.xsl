<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template name="stylesheets">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600" rel="stylesheet" type="text/css"/>
    <link href="{$workspace}/src/plugins/pace-master/themes/blue/pace-theme-flash.css" rel="stylesheet"/>
    <link href="{$workspace}/src/plugins/uniform/css/uniform.default.min.css" rel="stylesheet"/>
    <link href="{$workspace}/src/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="{$workspace}/src/plugins/fontawesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
    <link href="{$workspace}/src/plugins/line-icons/simple-line-icons.css" rel="stylesheet" type="text/css"/>	
    <link href="{$workspace}/src/plugins/offcanvasmenueffects/css/menu_cornerbox.css" rel="stylesheet" type="text/css"/>	
    <link href="{$workspace}/src/plugins/waves/waves.min.css" rel="stylesheet" type="text/css"/>	
    <link href="{$workspace}/src/plugins/switchery/switchery.min.css" rel="stylesheet" type="text/css"/>
    <link href="{$workspace}/src/plugins/3d-bold-navigation/css/style.css" rel="stylesheet" type="text/css"/>	
    <link href="{$workspace}/src/plugins/slidepushmenus/css/component.css" rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" type="text/css" href="{$workspace}/src/css/home.css"/>

    <link href="{$workspace}/src/css/modern.min.css" rel="stylesheet" type="text/css"/>
    <link href="{$workspace}/src/css/themes/green.css" class="theme-color" rel="stylesheet" type="text/css"/>
    <link href="{$workspace}/src/css/custom.css" rel="stylesheet" type="text/css"/>
 </xsl:template>

</xsl:stylesheet>