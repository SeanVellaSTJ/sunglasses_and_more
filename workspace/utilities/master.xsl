<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="javascript.xsl"/>
<xsl:import href="stylesheet.xsl"/>
<xsl:import href="footer-javascript.xsl"/>

<xsl:output method="xml"
	doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
	omit-xml-declaration="yes"
	encoding="UTF-8"
	indent="yes" />

	<xsl:template match="/">
		<html class="js flexbox canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths">
			<head>
		        <!-- Title -->
		        <title>Modern | Layouts - Blank Page</title>
		        
		        <meta content="width=device-width, initial-scale=1" name="viewport"/>
		        <meta charset="UTF-8"/>
		        <meta name="description" content="Admin Dashboard Template"/>
		        <meta name="keywords" content="admin,dashboard"/>
		        <meta name="author" content="Steelcoders"/>
		        
		       
		        
		        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		        <!--[if lt IE 9]>
		        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		        <![endif]-->
		        
		        <xsl:call-template name="stylesheets"/>
		        <xsl:call-template name="javascript"/>

			</head>
			<body class="page-header-fixed  pace-done">
				<div class="pace  pace-inactive">
					<div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
						<div class="pace-progress-inner"></div>
						</div>
						<div class="pace-activity"></div></div>
				        <div class="overlay"></div>
				        <!-- <div class="menu-wrap">
				            <nav class="profile-menu">
				                <div class="profile"><img src="assets/images/profile-menu-image.png" width="60" alt="David Green"><span>David Green</span></div>
				                <div class="profile-menu-list">
				                    <a href="#"><i class="fa fa-star"></i><span>Favorites</span></a>
				                    <a href="#"><i class="fa fa-bell"></i><span>Alerts</span></a>
				                    <a href="#"><i class="fa fa-envelope"></i><span>Messages</span></a>
				                    <a href="#"><i class="fa fa-comment"></i><span>Comments</span></a>
				                </div>
				            </nav>
				            <button class="close-button" id="close-button">Close Menu</button>
				        </div> -->
				        <form class="search-form" action="#" method="GET">
				            <div class="input-group">
				                <input type="text" name="search" class="form-control search-input" placeholder="Search..."/>
				                <span class="input-group-btn">
				                    <button class="btn btn-default close-search waves-effect waves-button waves-classic" type="button"><i class="fa fa-times"></i></button>
				                </span>
				            </div><!-- Input Group -->
				        </form><!-- Search Form -->
				        <main class="page-content content-wrap">
				            <div class="navbar">
				                <div class="navbar-inner">
				                    <div class="sidebar-pusher">
				                        <a href="javascript:void(0);" class="waves-effect waves-button waves-classic push-sidebar">
				                            <i class="fa fa-bars"></i>
				                        </a>
				                    </div>
				                    <div class="logo-box">
				                        <a href="index.html" class="logo-text"><span>Modern</span></a>
				                    </div><!-- Logo Box -->
				                    <div class="search-button">
				                        <a href="javascript:void(0);" class="waves-effect waves-button waves-classic show-search"><i class="fa fa-search"></i></a>
				                    </div>
				                    <div class="topmenu-outer">
				                        <div class="top-menu">
				                            <ul class="nav navbar-nav navbar-left">
				                                <li>		
				                                    <a href="javascript:void(0);" class="waves-effect waves-button waves-classic sidebar-toggle"><i class="fa fa-bars"></i></a>
				                                </li>
				                                <li>
				                                    <a href="#cd-nav" class="waves-effect waves-button waves-classic cd-nav-trigger"><i class="fa fa-diamond"></i></a>
				                                </li>
				                                <li>		
				                                    <a href="javascript:void(0);" class="waves-effect waves-button waves-classic toggle-fullscreen"><i class="fa fa-expand"></i></a>
				                                </li>
				                            </ul>
				                            <ul class="nav navbar-nav navbar-right">
				                                <li class="dropdown">
				                                    <a href="#" class="dropdown-toggle waves-effect waves-button waves-classic" data-toggle="dropdown">
				                                        <span class="user-name">David<i class="fa fa-angle-down"></i></span>
				                                    </a>
				                                    <ul class="dropdown-menu dropdown-list" role="menu">
				                                        <li role="presentation"><a href="profile.html"><i class="fa fa-user"></i>Profile</a></li>
				                                        <li role="presentation"><a href="calendar.html"><i class="fa fa-calendar"></i>Calendar</a></li>
				                                        <li role="presentation"><a href="inbox.html"><i class="fa fa-envelope"></i>Inbox<span class="badge badge-success pull-right">4</span></a></li>
				                                        <li role="presentation" class="divider"></li>
				                                        <li role="presentation"><a href="lock-screen.html"><i class="fa fa-lock"></i>Lock screen</a></li>
				                                        <li role="presentation"><a href="login.html"><i class="fa fa-sign-out m-r-xs"></i>Log out</a></li>
				                                    </ul>
				                                </li>
				                            </ul><!-- Nav -->
				                        </div><!-- Top Menu -->
				                    </div>
				                </div>
				            </div><!-- Navbar -->
				            <div class="page-sidebar sidebar">
				                <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100%;"><div class="page-sidebar-inner slimscroll" style="overflow: hidden; width: auto; height: 100%;">
				                    <div class="sidebar-header">
				                        <div class="sidebar-profile">
				                            <a href="javascript:void(0);" id="profile-menu-link">
				                                <div class="sidebar-profile-details">
				                                    <span>David Green<br/><small>Art Director</small></span>
				                                </div>
				                            </a>
				                        </div>
				                    </div>
				                    <ul class="menu accordion-menu">
				                        <li><a href="index.html" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-home"></span><p>Dashboard</p></a></li>
				                        <li><a href="profile.html" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-user"></span><p>Profile</p></a></li>
				                        <li class="droplink"><a href="#" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-envelope"></span><p>Mailbox</p><span class="arrow"></span></a>
				                            <ul class="sub-menu" style="display: none;">
				                                <li><a href="inbox.html">Inbox</a></li>
				                                <li><a href="inbox-alt.html">Inbox Alt</a></li>
				                                <li><a href="message-view.html">View Message</a></li>
				                                <li><a href="message-view-alt.html">View Message Alt</a></li>
				                                <li><a href="compose.html">Compose</a></li>
				                                <li><a href="compose-alt.html">Compose Alt</a></li>
				                            </ul>
				                        </li>
				                        
				                        <li class="droplink"><a href="#" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-briefcase"></span><p>UI Kits</p><span class="arrow"></span></a>
				                            <ul class="sub-menu" style="display: none;">
				                                <li><a href="ui-alerts.html">Alerts</a></li>
				                                <li><a href="ui-buttons.html">Buttons</a></li>
				                                <li><a href="ui-icons.html">Icons</a></li>
				                                <li><a href="ui-typography.html">Typography</a></li>
				                                <li><a href="ui-notifications.html">Notifications</a></li>
				                                <li><a href="ui-grid.html">Grid</a></li>
				                                <li><a href="ui-tabs-accordions.html">Tabs &amp; Accordions</a></li>
				                                <li><a href="ui-modals.html">Modals</a></li>
				                                <li><a href="ui-panels.html">Panels</a></li>
				                                <li><a href="ui-progress.html">Progress Bars</a></li>
				                                <li><a href="ui-sliders.html">Sliders</a></li>
				                                <li><a href="ui-nestable.html">Nestable</a></li>
				                                <li><a href="ui-tree-view.html">Tree View</a></li>
				                            </ul>
				                        </li>
				                        <li class="droplink active open"><a href="#" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-th"></span><p>Layouts</p><span class="arrow"></span></a>
				                            <ul class="sub-menu" style="display: block;">
				                                <li class="active"><a href="layout-blank.html">Blank Page</a></li>
				                                <li><a href="layout-boxed.html">Boxed Page</a></li>
				                                <li><a href="layout-horizontal-menu.html">Horizontal Menu</a></li>
				                                <li><a href="layout-horizontal-menu-boxed.html">Boxed &amp; Horizontal Menu</a></li>
				                                <li><a href="layout-horizontal-menu-minimal.html">Horizontal Menu Minimal</a></li>
				                                <li><a href="layout-fixed-sidebar.html">Fixed Sidebar</a></li>
				                                <li><a href="layout-static-header.html">Static Header</a></li>
				                                <li><a href="layout-collapsed-sidebar.html">Collapsed Sidebar</a></li>
				                                <li><a href="layout-compact-menu.html">Compact Menu</a></li>
				                                <li><a href="layout-hover-menu.html">Hover Menu</a></li>
				                            </ul>
				                        </li>
				                        <li class="droplink"><a href="#" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-list"></span><p>Tables</p><span class="arrow"></span></a>
				                            <ul class="sub-menu" style="display: none;">
				                                <li><a href="table-static.html">Static Tables</a></li>
				                                <li><a href="table-responsive.html">Responsive Tables</a></li>
				                                <li><a href="table-data.html">Data Tables</a></li>
				                            </ul>
				                        </li>
				                        <li class="droplink"><a href="#" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-edit"></span><p>Forms</p><span class="arrow"></span></a>
				                            <ul class="sub-menu" style="display: none;">
				                                <li><a href="form-elements.html">Form Elements</a></li>
				                                <li><a href="form-wizard.html">Form Wizard</a></li>
				                                <li><a href="form-upload.html">File Upload</a></li>
				                                <li><a href="form-image-crop.html">Image Crop</a></li>
				                                <li><a href="form-image-zoom.html">Image Zoom</a></li>
				                                <li><a href="form-select2.html">Select2</a></li>
				                                <li><a href="form-x-editable.html">X-editable</a></li>
				                            </ul>
				                        </li>
				                        <li class="droplink"><a href="#" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-stats"></span><p>Charts</p><span class="arrow"></span></a>
				                            <ul class="sub-menu" style="display: none;">
				                                <li><a href="charts-sparkline.html">Sparkline</a></li>
				                                <li><a href="charts-rickshaw.html">Rickshaw</a></li>
				                                <li><a href="charts-morris.html">Morris</a></li>
				                                <li><a href="charts-flotchart.html">Flotchart</a></li>
				                                <li><a href="charts-chartjs.html">Chart.js</a></li>
				                            </ul>
				                        </li>
				                        <li class="droplink"><a href="#" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-log-in"></span><p>Login</p><span class="arrow"></span></a>
				                            <ul class="sub-menu" style="display: none;">
				                                <li><a href="login.html">Login Form</a></li>
				                                <li><a href="login-alt.html">Login Alt</a></li>
				                                <li><a href="register.html">Register Form</a></li>
				                                <li><a href="register-alt.html">Register Alt</a></li>
				                                <li><a href="forgot.html">Forgot Password</a></li>
				                                <li><a href="lock-screen.html">Lock Screen</a></li>
				                            </ul>
				                        </li>
				                        <li class="droplink"><a href="#" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-map-marker"></span><p>Maps</p><span class="arrow"></span></a>
				                            <ul class="sub-menu" style="display: none;">
				                                <li><a href="maps-google.html">Google Maps</a></li>
				                                <li><a href="maps-vector.html">Vector Maps</a></li>
				                            </ul>
				                        </li>
				                        <li class="droplink"><a href="#" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-gift"></span><p>Extra</p><span class="arrow"></span></a>
				                            <ul class="sub-menu" style="display: none;">
				                                <li><a href="404.html">404 Page</a></li>
				                                <li><a href="500.html">500 Page</a></li>
				                                <li><a href="invoice.html">Invoice</a></li>
				                                <li><a href="calendar.html">Calendar</a></li>
				                                <li><a href="faq.html">FAQ</a></li>
				                                <li><a href="todo.html">Todo</a></li>
				                                <li><a href="pricing-tables.html">Pricing Tables</a></li>
				                                <li><a href="shop.html">Shop</a></li>
				                                <li><a href="gallery.html">Gallery</a></li>
				                                <li><a href="timeline.html">Timeline</a></li>
				                                <li><a href="search.html">Search Results</a></li>
				                                <li><a href="coming-soon.html">Coming Soon</a></li>
				                                <li><a href="contact.html">Contact</a></li>
				                            </ul>
				                        </li>
				                        <li class="droplink"><a href="#" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-flash"></span><p>Levels</p><span class="arrow"></span></a>
				                            <ul class="sub-menu" style="display: none;">
				                                <li class="droplink"><a href="#"><p>Level 1.1</p><span class="arrow"></span></a>
				                                    <ul class="sub-menu" style="display: none;">
				                                        <li class="droplink"><a href="#"><p>Level 2.1</p><span class="arrow"></span></a>
				                                            <ul class="sub-menu" style="display: none;">
				                                                <li><a href="#">Level 3.1</a></li>
				                                            </ul>
				                                        </li>
				                                        <li><a href="#">Level 2.2</a></li>
				                                    </ul>
				                                </li>
				                                <li><a href="#">Level 1.2</a></li>
				                            </ul>
				                        </li>
				                    </ul>
				                </div><div class="slimScrollBar" style="width: 7px; position: absolute; top: 0px; opacity: 0.3; display: none; border-radius: 0px; z-index: 99; right: 0px; height: 1516px; background: rgb(204, 204, 204);"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 0px; opacity: 0.2; z-index: 90; right: 0px; background: rgb(51, 51, 51);"></div></div><!-- Page Sidebar Inner -->
				            </div><!-- Page Sidebar -->
				            <div class="page-inner" style="min-height:1576px !important">
				                <div class="page-title">
				                    <h3><xsl:value-of select="/data/params/current-page"/></h3>
				                </div>
				                <div id="main-wrapper">
				                    <div class="row">

				                    	<xsl:apply-templates select="/data"/>

				                    </div><!-- Row -->
				                </div><!-- Main Wrapper -->
				                <div class="page-footer">
				                    <p class="no-s">2015 © Modern by Steelcoders.</p>
				                </div>
				            </div><!-- Page Inner -->
				        </main><!-- Page Content -->
				        <nav class="cd-nav-container" id="cd-nav">
				            <header>
				                <h3>Navigation</h3>
				                <a href="#0" class="cd-close-nav">Close</a>
				            </header>
				            <ul class="cd-nav list-unstyled">
				                <li class="cd-selected" data-menu="index">
				                    <a href="javsacript:void(0);">
				                        <span>
				                            <i class="glyphicon glyphicon-home"></i>
				                        </span>
				                        <p>Dashboard</p>
				                    </a>
				                </li>
				                <li data-menu="profile">
				                    <a href="javsacript:void(0);">
				                        <span>
				                            <i class="glyphicon glyphicon-user"></i>
				                        </span>
				                        <p>Profile</p>
				                    </a>
				                </li>
				                <li data-menu="inbox">
				                    <a href="javsacript:void(0);">
				                        <span>
				                            <i class="glyphicon glyphicon-envelope"></i>
				                        </span>
				                        <p>Mailbox</p>
				                    </a>
				                </li>
				                <li data-menu="#">
				                    <a href="javsacript:void(0);">
				                        <span>
				                            <i class="glyphicon glyphicon-tasks"></i>
				                        </span>
				                        <p>Tasks</p>
				                    </a>
				                </li>
				                <li data-menu="#">
				                    <a href="javsacript:void(0);">
				                        <span>
				                            <i class="glyphicon glyphicon-cog"></i>
				                        </span>
				                        <p>Settings</p>
				                    </a>
				                </li>
				                <li data-menu="calendar">
				                    <a href="javsacript:void(0);">
				                        <span>
				                            <i class="glyphicon glyphicon-calendar"></i>
				                        </span>
				                        <p>Calendar</p>
				                    </a>
				                </li>
				            </ul>
				        </nav>
				        <div class="cd-overlay"></div>

				       <xsl:call-template name="javascript-footer"/>
				</body>
		</html>
	</xsl:template>


</xsl:stylesheet>