<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template name="javascript-footer">
	 <!-- Javascripts -->
    <script src="{$workspace}/src/plugins/jquery/jquery-2.1.4.min.js"></script>
    <script src="{$workspace}/src/plugins/jquery-ui/jquery-ui.min.js"></script>
    <script src="{$workspace}/src/plugins/pace-master/pace.min.js"></script>
    <script src="{$workspace}/src/plugins/jquery-blockui/jquery.blockui.js"></script>
    <script src="{$workspace}/src/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="{$workspace}/src/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script src="{$workspace}/src/plugins/switchery/switchery.min.js"></script>
    <script src="{$workspace}/src/plugins/uniform/jquery.uniform.min.js"></script>
    <script src="{$workspace}/src/plugins/offcanvasmenueffects/js/classie.js"></script>
    <script src="{$workspace}/src/plugins/offcanvasmenueffects/js/main.js"></script>
    <script src="{$workspace}/src/plugins/waves/waves.min.js"></script>
    <script src="{$workspace}/src/plugins/3d-bold-navigation/js/main.js"></script>
    <script src="{$workspace}/src/js/modern.min.js"></script>
</xsl:template>

</xsl:stylesheet>