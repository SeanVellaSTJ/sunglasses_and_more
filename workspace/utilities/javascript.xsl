<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template name="javascript">
	<!-- Theme Styles -->
	<script src="{$workspace}/src/plugins/3d-bold-navigation/js/modernizr.js"></script>
	<script src="{$workspace}/src/plugins/offcanvasmenueffects/js/snap.svg-min.js"></script>
</xsl:template>

</xsl:stylesheet>